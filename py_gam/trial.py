import pygame
import math
import numpy 


#initiate pygame
pygame.init()


#clock
clock=pygame.time.Clock()


#RGB Colors
WHITE =(255,255,255)
BLACK =(0,0,0)
BLUE =(0,0,255)
CYAN =(0,255,255)
RED = (255, 0, 0)
RED = (0, 255, 0)
PURPLE = (255, 0,255)
YELLOW = (255, 255, 0)



#window setup


wn_width = 1000
wn_height = 1000

BORDER = pygame.Rect(wn_width//2 - 5 ,0,10,wn_height)
wn = pygame.display.set_mode((wn_width, wn_height))
pygame.display.set_caption("Drawing Shapes")







def draw_dashed_line(surf, color, start_pos, end_pos, width=1, dash_length=10):
    x1, y1 = start_pos
    x2, y2 = end_pos
    dl = dash_length

    if (x1 == x2):
        ycoords = [y for y in range(y1, y2, dl if y1 < y2 else -dl)]
        xcoords = [x1] * len(ycoords)
    elif (y1 == y2):
        xcoords = [x for x in range(x1, x2, dl if x1 < x2 else -dl)]
        ycoords = [y1] * len(xcoords)
    else:
        a = abs(x2 - x1)
        b = abs(y2 - y1)
        c = round(math.sqrt(a**2 + b**2))
        dx = dl * a / c
        dy = dl * b / c

        xcoords = [x for x in numpy.arange(x1, x2, dx if x1 < x2 else -dx)]
        ycoords = [y for y in numpy.arange(y1, y2, dy if y1 < y2 else -dy)]

    next_coords = list(zip(xcoords[1::2], ycoords[1::2]))
    last_coords = list(zip(xcoords[0::2], ycoords[0::2]))
    for (x1, y1), (x2, y2) in zip(next_coords, last_coords):
        start = (round(x1), round(y1))
        end = (round(x2), round(y2))
        pygame.draw.line(surf, color, start, end, width)




#main loop
state=True
while state:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            state=False
            

    wn.fill(BLACK)
    #LINE
    startX=0; startY=250; endX=wn_width; endY=150; width=5; dash_length=5
    
    

    
    









    # pygame.draw.line(wn,BLACK, (startX, startY), (endX, endY))
    # draw_dashed_line(wn, WHITE, (250, 0), (0, 1300), dash_length=5)


    
    draw_dashed_line(wn, WHITE, (0, 450), (500, 450), dash_length=5)
    draw_dashed_line(wn, WHITE, (0, 400), (500, 400), dash_length=5)
    draw_dashed_line(wn, WHITE, (0, 350), (500, 350), dash_length=5)
    draw_dashed_line(wn, WHITE, (0, 300), (500, 300), dash_length=5)
    draw_dashed_line(wn, WHITE, (0, 250), (500, 250), dash_length=5) #reference point


    # pygame.draw.arc(wn, WHITE,)

    pygame.draw.circle(wn, YELLOW, [600, 350], 40)
    pygame.draw.circle(wn, "green", [600, 350], 30)



    # pygame.draw.arc(wn, (255, 255, 255), (500, 250, 300, 200), math.pi/180, math.pi, 1)
    # pygame.draw.arc(wn, (255, 255, 255), (500, 300, 200, 200), math.pi/180, math.pi, 1)
    # pygame.draw.arc(wn, (255, 255, 255), (500, 350, 200, 200), math.pi/180, math.pi, 1)
    # pygame.draw.arc(wn, (255, 255, 255), (500, 200, 200, 200), math.pi/180, math.pi, 1)
    # pygame.draw.arc(wn, (255, 255, 255), (500, 150, 200, 200), math.pi/180, math.pi, 1)









    

   

    print(BORDER)













    pygame.display.update()
    clock.tick(30)



#pygame quit
pygame.quit()
quit()