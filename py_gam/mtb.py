import pygame 
import os
import math
import numpy as np
pygame.font.init()
pygame.mixer.init()


WIDTH, HEIGHT = 900,500
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("First Game!")
WHITE =(255,255,255)
BLACK =(0,0,0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)
screen = pygame.display.set_mode((1920, 1080))


FPS = 60


SPACESHIP_WIDTH, SPACESHIP_HEIGHT =55,40

YELLOW_HIT = pygame.USEREVENT +1
RED_HIT = pygame.USEREVENT +2


YELLOW_SPACESHIP_IMAGE = pygame.image.load(os.path.join('Assets', 'spaceship_yellow.png'))
YELLOW_SPACESHIP_IMAGE =  pygame.transform.rotate(pygame.transform.scale(
YELLOW_SPACESHIP_IMAGE, (SPACESHIP_WIDTH,SPACESHIP_HEIGHT)),90)



RED_SPACESHIP_IMAGE = pygame.image.load(os.path.join('Assets', 'spaceship_red.png'))
RED_SPACESHIP_IMAGE = pygame.transform.rotate(pygame.transform.scale(
RED_SPACESHIP_IMAGE, (SPACESHIP_WIDTH,SPACESHIP_HEIGHT)), 270)


SPACE = pygame.transform.scale(pygame.image.load(os.path.join('Assets', 'space.png')), (WIDTH, HEIGHT) )





def draw_window(red,yellow, red_bullets, yellow_bullets, red_health, yellow_health):
    WIN.fill(WHITE)
    # WIN.blit(SPACE,(0, 0) )
    # pygame.draw.rect(WIN, BLACK, BORDER )
    # pygame.draw.rect(WIN, BLACK, BORDER_2 )


    # red_health_text= HEALTH_FONT.render("Health:" + str(red_health), 1, WHITE)
    # yellow_health_text= HEALTH_FONT.render("Health:" + str(yellow_health), 1, WHITE)


    # WIN.blit(red_health_text, (WIDTH - red_health_text.get_width()-10, 10))

    # WIN.blit(yellow_health_text, ( 10, 10))






    # WIN.blit(YELLOW_SPACESHIP_IMAGE, (yellow.x,yellow.y))
    # WIN.blit(RED_SPACESHIP_IMAGE, (red.x,red.y))




    for bullet in red_bullets:
        pygame.draw.rect(WIN, RED, bullet)

    for bullet in yellow_bullets:
        pygame.draw.rect(WIN, YELLOW, bullet)


    pygame.display.update()


def draw_line_dashed(surface, color, start_pos, end_pos, width = 1, dash_length = 10, exclude_corners = True):

    # convert tuples to numpy arrays
    start_pos = np.array(start_pos)
    end_pos   = np.array(end_pos)

    # get euclidian distance between start_pos and end_pos
    length = np.linalg.norm(end_pos - start_pos)

    # get amount of pieces that line will be split up in (half of it are amount of dashes)
    dash_amount = int(length / dash_length)

    # x-y-value-pairs of where dashes start (and on next, will end)
    dash_knots = np.array([np.linspace(start_pos[i], end_pos[i], dash_amount) for i in range(2)]).transpose()

    return [pygame.draw.line(surface, color, tuple(dash_knots[n]), tuple(dash_knots[n+1]), width)
            for n in range(int(exclude_corners), dash_amount - int(exclude_corners),2)]






def main():

    red_bullets = []
    yellow_bullets = []

    red = pygame.Rect(700, 300,SPACESHIP_WIDTH,SPACESHIP_HEIGHT)
    yellow = pygame.Rect(100, 300,SPACESHIP_WIDTH,SPACESHIP_HEIGHT)



    red_health = 10
    yellow_health = 10




    #main game loop-->infinite loop
    clock = pygame.time.Clock()
    run= True

    while run:
        clock.tick(FPS)
        draw_window(red, yellow, red_bullets, yellow_bullets, red_health, yellow_health)
        draw_line_dashed(screen, RED, (0, 0), (800, 600), dash_length=5)


        # handle_bullets(yellow_bullets, red_bullets, yellow, red, )

      

    
    pygame.quit()


if __name__== "__main__":
    main()


